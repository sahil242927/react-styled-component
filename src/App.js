import React from "react";
import "./App.css";
import { ThemeProvider } from "styled-components";
import Button from "./elements/Button";

const theme = {
  primary: "teal",
  secondary: "magenta",
  font: "sans-serif",
  alert: "yellow"
};

function App() {
  return (
    <ThemeProvider theme={theme}>
      <div className="App">
        <Button primary color="alert">
          Create
        </Button>
      </div>
    </ThemeProvider>
  );
}

export default App;
