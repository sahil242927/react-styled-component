import styled, { ThemeProvider, css } from "styled-components";

export default styled.button`
  font-family: ${props => props.theme.font};
  font-size: 1.3rem;
  border: none;
  border-radius: 5px;
  background-color: ${props => props.theme.font};
  ${props =>
    props.color &&
    css`
      background: ${props => props.theme[props.color]};
    `};
  padding: 1rem;
  &:hover {
    background-color: orange;
    color: white;
  }
`;
